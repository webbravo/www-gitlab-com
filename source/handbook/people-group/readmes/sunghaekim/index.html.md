---
layout: handbook-page-toc
title: "Sung Hae Kim's README"
---

## Intro

This page shares information specific to Sung Hae Kim, Chief People Officer of GitLab. This page is intended to be helpful to individuals who interact with Sung Hae (especially her direct reports and team members in her organization). Feel free to deviate from it (and feel free to be yourself when interacting with her) and submit a merge request on this page to make it more useful. If there are things that might seem confusing, narcissistic, pretentious, or overbearing please raise your concerns so we can remove or adapt them. 

## Related pages

- Chief People Officer [Job Description](https://about.gitlab.com/job-families/people-ops/chief-people-officer/)

## Flaws

- Transparency and directness are part of our [values](https://about.gitlab.com/handbook/values/) and I want to live by them by sharing and improving my flaws. I am listing my flaws for two main reasons: 1) So that people know the behavior is not about them but about me and how I communicate or process information; 2) So that I can get feedback from others and improve on areas that are important to team members. I want people to know that, not only do I appreciate when they speak up by providing me with direct feedback, I believe this builds greater trust and respect.

- I have a tendency to want to get results efficiently and quickly, so I need to work on being more patient. Related to this, I have been given feedback that I have interrupted people during meetings and this is an improvement area for me in order to have more inclusive interactions. If I interrupt someone while they are speaking, it’s probably because of one of the following reasons: a) I’m very enthusiastic about the topic (and I’ve probably had too much caffeine), b) I feel we have gotten off track and I want to get back on track, c) I am anxious that we are running out of the allotted time for a meeting and we haven’t discussed the important items on the meeting agenda, or d) I feel the speaker has already made their point and we need to move on. These are explanations, not excuses. If I have cut someone off and they are offended or have something important to say, please let me know immediately and directly. Also, I genuinely don’t mind being cut off while speaking - unless I am in the middle of making an important point, and I’ll let the person know when I need to finish speaking. This means that I need to remember to apply the [platinum rule](https://www.inc.com/peter-economy/how-the-platinum-rule-trumps-the-golden-rule-every-time.html) (treat others as they want to be treated) instead of the [golden rule](https://en.wikipedia.org/wiki/Golden_Rule).

- I have also received feedback that I can be more terse in text communication than in-person communication, especially when I’m texting while multitasking or when I’m in a hurry. Short, to-the-point texts can occasionally lead people to think my message is negative or severe. To help soften the tone with short messages, I often use all lowercase text but this does not often translate well. In order to avoid misunderstandings, I will try to make use of emoticons and check-in with people to make sure my messaging comes across as clear/to-the-point, yet not severe or negative.

- I generally have a positive, can-do attitude. In order to prevent myself from heading off in the wrong direction, I need and value people who think differently than I do versus people who agree with me as their default. This means that I also think about the composition of my team and try to ensure we have a diversity of thinking styles (e.g., those who like to reflect longer on a decision). I also need to consciously create space for differing opinions and allow people with different communication/thinking styles to let me know if they need something different from me (e.g., more time to process, more/less direction). 

- I like to be more lighthearted than serious. If I say something that is humorous (at least in my mind)  or irreverent and this is confusing, distracting, or annoying to someone, I would really want to know and I will try to adjust my style when communicating with you.

- I value feedback and, like most people, prefer actionable, specific, kind, timely and direct feedback. I even welcome on-the-spot public, corrective feedback (even during a meeting in front of many others) - as long as it is done with positive intent.  It makes me very sad to be the last person to know something negative (or something that needs improvement) about me. I can actually lose sleep over things like that because I’m transported back to feelings of being alienated as a 13 year old in middle school. I do need to improve in this area and not dwell about people not being comfortable about giving me direct feedback.

- When discussing anything related to decision making, feedback, or action planning, my preference is to debate with specific examples, facts, and figures over feelings and generalizations that may be based on a couple of incidents. Comments like “Everyone thinks…” tend to throw me off because I like to have facts to back up conclusions. Since feelings (and validating them) are so important, I need to be more patient when people share general feelings or reactions without examples or facts.

- I am often multi-tasking in my head. If I have a pensive look or frown on my face, it’s often about something unrelated to what’s going on in the meeting (e.g., Am I late for another meeting?, I forgot to buy abc, I need to take my dog out, I need to call my son about xyz). If my facial expression is concerning or confusing, I welcome people checking in with me instead of assuming something negative.

- I may correct words that someone uses about me or our work, especially when I feel the words seem inaccurate or pretentious. When I forget to explain why I don’t prefer certain words, this can make the person feel like they’ve done something wrong. I need to get better about always sharing context.

Not a flaw but something to know about me, I care more about my team and our success than I do about my own success. It would worry me if people tried to guess, fuss about, or focus on what they think I’d want over what they think is the right thing for them, the team, and the company. 

## Communication

Here are my preferences for ad hoc communications with me:

- Not confidential: Slack @sunghaekim in a public channel
- Confidential: Slack @sunghaekim in a private channel (create a private channel and invites others as necessary, if needed)
- Confidential to me only: DM Slack @sunghaekim
- Social/fun: Any and all modes of communication are fine!
- Urgent, important, needs conversation: /zoom on Slack


## Other
