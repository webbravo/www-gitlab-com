---
layout: handbook-page-toc
title: "UseCase driven GTM"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## UseCase driven Go to market

The [Customer Use-Cases](/handbook/use-cases/) page defines 8 customer use cases which are the typical problems that a customer selects Gitlab to solve. In product marketing we a focus on GitLab stages **and** we also develop and curate collateral that aligns with a buyer's journey supporting the use cases.

## Buyer's journey and typical collateral

As a prospect is determining that they have a specific problem to solve, they typically go through several stages of a [buyer's journey](/handbook/marketing/corporate-marketing/content/#content-stage--buyers-journey-definitions) which we hope ultimately leads them to selecting GitLab as their solution of choice.

| **Awareness** | **Consideration** | **Decision/Purchase** |
|----|----|----|
| Collateral and content designed to reach prospects in this stage of their journey should be focused on **educating them about the problems they are facing**, the business impact of their problems, and the reality that others are successfully solving the same problem | Collateral and content designed to reach prospects in this stage of their journey should be focused on **positioning GitLab as a viable and compelling solution to their specific problem.** | Collateral and content designed to reach prospects in this stage of their journey should be focused on key information that a buyer needs to **justify GitLab as their chosen solution**. |
| Typical **Awareness** Collateral | Typical **Consideration** Collateral | Typical **Decision/Purchase** Collateral |
|  **-** White papers describing the problem space <br> **-** Infographics illustrating the impact of the problem/challenge <br> **-** Analyst reports describing the problem/domain  <br> **-** Webinars focusing on the problem and how can be solved <br> **-** Troubleshooting guides to help overcome the problem  <br> **-** Analysis of public cases where the problem impacted an organization (i.e. outage, data loss, etc) | **-** White papers describing the innovative solutions to the problem <br> **-** Infographics illustrating the success and impact of solving the problem <br> **-** Analyst reports comparing different solutions in the market (MQ, Waves, etc) <br> **-** Webinars focusing on the success stories and how gitlab helped solve the problem <br> **-** Customer Case Studies, Videos, Logos, etc <br> **-** Solution Check Lists / Plans for how to solve the problem <br> **-** Comparisons between GitLab and other solutions | **-** ROI calculators <br> **-** Use case specific implementation guides <br> **-** Use Case migration guides (from xyz to GitLab) <br> **-** Getting Started info <br> **-** References and case studies |

## Buyer's Journey - Audience

There are different audiences in an organization with different needs at different times. The general model for the buyer's journey outlines kinds of collateral that is needed at different stages of the buying cycle. It is incredibly important to understand the needs of the audience when creating collateral. Executives, managers, and individual contributors will need different information to support their specific work.

![Buyer's Journey](./buyers-cycle-journey.png)

- **Executives** will need information about business value, risk, cost, and impact to support strategic objectives
- **Managers** will need more detailed information to drive planning, justification, and migration details.
- **Contributors** need technical details about how it works, how it's better, and how it's going to help them in the future.

### UseCase GTM Bill of Material Priorities

| Priority | Area | Collateral/Activity | Specifics |
|----------|-------------------|---------------------------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------|
| 1 | Customer Insights | Personas | User and Buyer personas definition |
| 1 | Market Research | Use Case Capabilities | Decompose the market use case into the key 5 to 10 'capabilities' that the market defines.  (input from analyst research, competition, market research, etc) |
| 1 | Market Research | Analyst Landscape | Analyst: Identify key analysts, current published research, inquiry & briefing plan |
| 1 | UseCase Strategy | Positioning | GTM Framework (messaging, positioning, differentiation, etc) |
| 1 | Awareness | Analyst Influence about GitLab Usecase | Analyst Reports (Thought Leadership, Comparative, etc.) |
| 1 | Awareness | Web content/landing page | Use case specific webpage |
| 1 | Awareness | Partner (optional) | Joint datasheet, documentation (from both GL and Partners) |
| 1 | Awareness | Partner (optional) | Partner specific Campaign |
| 1 | Awareness | Technical Marketing Assets | Use case demo |
| 1 | Consideration | Customer Interaction | Customer discovery questions |
| 1 | Consideration | Trial/Evaluation/Technical Marketing Assets | Trial use case Quickstart Guide |
| 1 | Consideration | Use Case specific CFD | Including PS packages and roadmap |
| 1 | Consideration | Competitive Comparisons | Use Case Capabilities |
| 1 | Consideration | Partner (optional) | Joint technology demos (partner-related use cases) |
| 1 | Consideration | Differentiation | One solid case study |
| 1 | Decision | Business Case | ROI |
| 1 | Decision | Differentiation | 5 Customer References various industries |
| 2 | Awareness | Web Content | Webinar |
| 2 | Awareness | Web Content | Technical Webinar |
| 2 | Awareness | Campaigns | Campaign Content/Messaging |
| 2 | Awareness | Comparison pages |  |
| 2 | Awareness | Differentiation | 3 Customer speakers |
| 2 | Awareness | Technical Marketing Assets | Practitioner level conference presentations |
| 2 | Awareness | Partner (optional) | Webinars |
| 2 | Awareness | Partner (optional) | Thought leadership/customer conference presentations |
| 2 | Consideration | Customer Interaction | Use case trap setting questions (differentiation) |
| 2 | Consideration | Trial/Evaluation | Competitive demos |
| 2 | Consideration | Technical Marketing Assets | Capability demos (including differentiation) |
| 2 | Consideration | Technical Marketing Assets | Tech blogs on usage of target configuration |
| 2 | Consideration | Differentiation | 3 Cases studies - one per value driver for use case |
| 2 | Decision | Technical Marketing Assets | Migration guides |
| 2 | Enablement | Technical Marketing Assets | Sales demo enablement |
| 2 | Market Research | Market Estimation | TAM (Estimate where not specific) |
| 3 | Awareness | Web Content | Blogs |
| 3 | Awareness | Web Content | Thought Leadership |
| 3 | Consideration | Technical Marketing Assets | Practitioner level conference workshops |
| 3 | Market Research | Primary and/or Secondary Research Results (ex. Commissioned survey, CAB poll, etc) |  |

https://docs.google.com/spreadsheets/d/1K78pI_sqS91o8fpMReCF5n7gf7tZjCFxU4PjX2uddxk/edit#gid=0

### Use Case Assessment grid

| Priority | Area | Collateral/Activity | Specifics | SCM | CI | CD | Shift Left Security / DevSecOps | Agile | Simplify DevOps | Cloud Native | GitOps / IAC |
|----------|-------------------|---------------------------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------|-----|----|----|---------------------------------|-------|-----------------|--------------|--------------|
| 1 | Customer Insights | Personas | User and Buyer personas definition |  |  |  |  |  |  |  |  |
| 1 | Market Research | Use Case Capabilities | Decompose the market use case into the key 5 to 10 'capabilities' that the market defines.  (input from analyst research, competition, market research, etc) |  |  |  |  |  |  |  |  |
| 1 | Market Research | Analyst Landscape | Analyst: Identify key analysts, current published research, inquiry & briefing plan |  |  |  |  |  |  |  |  |
| 1 | UseCase Strategy | Positioning | GTM Framework (messaging, positioning, differentiation, etc) |  |  |  |  |  |  |  |  |
| 1 | Awareness | Analyst Influence about GitLab Usecase | Analyst Reports (Thought Leadership, Comparative, etc.) |  |  |  |  |  |  |  |  |
| 1 | Awareness | Web content/landing page | Use case specific webpage |  |  |  |  |  |  |  |  |
| 1 | Awareness | Partner (optional) | Joint datasheet, documentation (from both GL and Partners) |  |  |  |  |  |  |  |  |
| 1 | Awareness | Partner (optional) | Partner specific Campaign |  |  |  |  |  |  |  |  |
| 1 | Awareness | Technical Marketing Assets | Use case demo |  |  |  |  |  |  |  |  |
| 1 | Consideration | Customer Interaction | Customer discovery questions |  |  |  |  |  |  |  |  |
| 1 | Consideration | Trial/Evaluation/Technical Marketing Assets | Trial use case Quickstart Guide |  |  |  |  |  |  |  |  |
| 1 | Consideration | Use Case specific CFD | Including PS packages and roadmap |  |  |  |  |  |  |  |  |
| 1 | Consideration | Competitive Comparisons | Use Case Capabilities |  |  |  |  |  |  |  |  |
| 1 | Consideration | Partner (optional) | Joint technology demos (partner-related use cases) |  |  |  |  |  |  |  |  |
| 1 | Consideration | Differentiation | One solid case study |  |  |  |  |  |  |  |  |
| 1 | Decision | Business Case | ROI |  |  |  |  |  |  |  |  |
| 1 | Decision | Differentiation | 5 Customer References various industries |  |  |  |  |  |  |  |  |
