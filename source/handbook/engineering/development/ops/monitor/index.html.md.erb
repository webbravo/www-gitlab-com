---
layout: handbook-page-toc
title: "Monitor Stage"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Groups

The groups within this stage are:

* [APM](apm/)
* [Health](health/)

## Common links

* Slack channel: [#g_monitor](https://gitlab.slack.com/archives/g_monitor)
* Slack alias: @monitor-group
* Google group: monitor-stage@gitlab.com

## Vision

Using GitLab, you automatically get broad and deep insight into the health of your deployment.

## Mission

We provide a robust monitoring solution to give GitLab users insight into the performance and availability of their deployments and alert them to problems as soon as they arise. We provide data that is easy to digest and to relate to other features in GitLab. With every piece of the devops lifecycle integrated into GitLab, we have a unique opportunity to closely tie our monitoring features to all of the other pieces of the devops flow.

We work collaboratively and transparently and we will contribute as much of our work as possible back to the open source community.

## Responsibilities
{: #monitoring}

The monitoring team is responsible for:
* Providing the tools required to enable monitoring of GitLab.com
* Packaging these tools to enable all customers to manage their instances easily and completely
* Building integrated monitoring solutions for customers apps into GitLab, including: metrics, logging, and tracing

This team maps to [Monitor Stage](/handbook/product/categories/#monitor-stage).

## Async Daily Standups
We use the [geekbot slack plugin](https://geekbot.io/) to automate our async standup, following the guidelines outlined in the [Geekbot commands guide](https://geekbot.com/guides/commands/). Answers are concise and focused on top priority items. All question prompts are optional and only answered when the information should be surfaced to the team:

- **Are you facing any blockers requiring action from others?**

  Why do we ask this question?
    - Help ensure that we are unblocking one another as a top priority
    - We ask this first because we think it's the question that other team members can take action on

  What do we hope to achieve?
    - Improve collaboration and efficiency within the team

- **What are you aiming to accomplish by the end of the week?**

  We want to understand how our daily actions drive us toward our weekly goals. This question provides broader context for our daily work, but also helps us hold ourselves accountable to maintaining proper scopes for our tasks, issues, merge requests, etc. This answer may stay the same for a week, this would mean things are progressing on schedule. Alternatively, seeing this answer change throughout the week is also okay. Maybe we got side tracked helping someone get unblocked. Maybe new blockers came up. The intention is not to have to justify our actions, but to keep a running record of how our work is progressing or evolving.

- **What will be your primary focus for today?**

  This question is aimed at the most impactful task for the day. We aren't tyring to account for the entire day's worth of work. Highlighting only a primary task keeps our answers concise and provides insight into each team member's most important priority. This doesn't necessarily mean sharing the task that will take the most time. We focus on results over input. Typically this will mean highlighting the task that is most impactful in closing the gap between today and our end of the week goal(s).

- **Any personal tidbits you'd like to share?**

  This question is intentionally open ended. You might want to share how you feel, a personal anecdote, funny joke, or simply let the team know that you will have limited availability that afternoon. All of these answers are welcome.

## Recurring Meetings
Every-other week we have a [Monitor Stage Demo Hour](https://docs.google.com/document/d/1bmJJEColEupwCVY_UFXQE_0ATGEPoQL0H9COwg52-J4/edit#) for engineering and design demos by members of the Monitor Stage group. Demos are voluntary and on a sign-up basis.

There is also an optional Monitor Social Hour meeting every week. This call has no agenda and alternates times every other week to be more inclusive of team members in different time zones.

The [Health](health/) and [APM](apm/) groups have their own regular meetings as well.

## Retrospective
We follow the same retrospective process as the rest of the engineering department, which [can be found here](/handbook/engineering/management/team-retrospectives/).

To encourage a more iterative retrospective process, we create a new retrospective issue at the beginning of each milestone, using the [Monitor retrospective template](https://gitlab.com/gl-retrospectives/monitor/blob/master/.gitlab/issue_templates/Retrospective.md). We leave this issue open for the duration of the milestone so any team member can add feedback as it happens instead of waiting until the end of the milestone.

## Monitor Stage PTO
Just like the rest of the company, we use [PTO Ninja](/handbook/paid-time-off/#pto-ninja) to track when team members are traveling, attending conferences, and taking time off. The easiest way to see who has upcoming PTO is to run the `/ninja whosout` command in the `#g_monitor_standup` slack channel. This will show you the upcoming PTO for everyone in that channel.

## Useful Resources

* For an introduction to Prometheus, see this guide: [https://www.youtube.com/watch?v=8Ai55-sYJA0](https://www.youtube.com/watch?v=8Ai55-sYJA0).
* For setting up a kubernetes cluster for local development, consult this tutorial: [https://www.youtube.com/watch?v=dFIlml7O2go](https://www.youtube.com/watch?v=dFIlml7O2go).
* To run GitLab Omnibus in docker with common monitor-related features enabled, see this snippet: [https://gitlab.com/snippets/1892700](https://gitlab.com/snippets/1892700).
* To easily trigger Prometheus alerts locally, check out this project: [https://gitlab.com/gitlab-org/monitor/manual_prometheus](https://gitlab.com/gitlab-org/monitor/manual_prometheus).
* Other resources may also be available in the monitor group's namespace on [GitLab.com](https://gitlab.com/gitlab-org/monitor) or [Staging](https://staging.gitlab.com/gitlab-org/monitor).
